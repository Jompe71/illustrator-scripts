#target illustrator

// Globals.
var ADOBE_ILLUSTRATOR_EXT = ".ai";

/**
 * Gets the scaling factor of the export to PNG.
 * @param desiredOutputWidth Desired output witdh in pixels.
 * @returns Scaling factor. Default scaling factor is 100 which is also the return value 
 * 		    in case of no active document.
 */
function getScalingFactor( desiredOutputWidth ) {

	var openDocument = app.activeDocument,
		sf = 100.0;

	if( openDocument !== null ) {
		sf = desiredOutputWidth * 100 / openDocument.width;	
	}	
	$.writeln( "Desired output width of " + desiredOutputWidth + " px results in scaling factor " + sf );
	return sf; // As-is.
};

/**
 * Gets the illustrator files selected by the user.
 * @returns Array of files to export.
 */
function getIllustratorFilesInDir() {

	var selectedFileOrFolder = new Folder( "~" ).selectDlg( "Select source file or directory..." );
	var selectedFiles = new Array();

	if( selectedFileOrFolder !== null ) {
		if( ( selectedFileOrFolder instanceof File ) && 
			selectedFileOrFolder.fullName.endsWith( ADOBE_ILLUSTRATOR_EXT ) ) {
			selectedFiles.push( selectedFileOrFolder );
		}	
		else {
			var filesFound = selectedFileOrFolder.getFiles( "*" + ADOBE_ILLUSTRATOR_EXT );
			for( var ff = 0; ff < filesFound.length; ff++ ) {
				selectedFiles.push( filesFound[ ff ] );	
			}			
		}
	}
	return selectedFiles;
};

/**
 * Exports a file to SVG format.
 * @param file File to export as SVG.
 * @returns Full filename of newly exported.
 */
function exportFileAsSVG( file ) {
	
	// Construct a new filename, based upon the old one.
	var nfn = makeOutputFilenameAndroidResourceCompatible( file );

	// Switch extension.
	nfn = nfn.substring( 0, nfn.lastIndexOf( "." ) ) + ".svg";
	
	// Open source file, export and close.
	var doc = app.open( file, DocumentColorSpace.RGB );
	doc.activate();

	// Specify export options.
	var exportOptions = new ExportOptionsSVG();
	exportOptions.embedRasterImages = true;
	exportOptions.embedAllFonts = false;
	exportOptions.fontSubsetting = SVGFontSubsetting.GLYPHSUSED;

	doc.exportFile( new File( nfn ), ExportType.SVG, exportOptions );
	doc.close( SaveOptions.DONOTSAVECHANGES );
	doc = null;
	return nfn;
};

/**
 * Exports a file to PNG 24bit format.
 * @param file File to export as PNG.
 * @returns Full filename of newly exported.
 */
function exportFileAsPNG( file, desiredOutputWidth ) {

	// Construct a new filename, based upon the old one.
	var nfn = makeOutputFilenameAndroidResourceCompatible( file );

	// Append px width to name and switch extension.
	nfn = nfn.substring( 0, nfn.lastIndexOf( "." ) ) + "_" + desiredOutputWidth + ".png";
	
	// Open source file, export and close.
	var doc = app.open( file, DocumentColorSpace.RGB );
	doc.activate();

	// Specify export options.
	var exportOptions = new ExportOptionsPNG24();
	exportOptions.transparency = false;
	exportOptions.horizontalScale = exportOptions.verticalScale = getScalingFactor( desiredOutputWidth );
	
	doc.exportFile( new File( nfn ), ExportType.PNG24, exportOptions );
	doc.close( SaveOptions.DONOTSAVECHANGES );
	doc = null;
	return nfn;
};

/**
 * Makes a filename compatible with the android resoirce naming convetion.
 * I.e. only a-z, 0-9 and underscore.
 * Example:
 * JompeMinifiedFile With SpaceAndNumb3r.svg
 * becomes:
 * jompe_minified_file_with_space_and_numb3r.svg
 * @param original filename.
 * @return new filename.
 */
function makeOutputFilenameAndroidResourceCompatible( originalFile ) {

	// Get the name of the file, i.e. skip working on the path.
	var originalFilename = originalFile.name;

	// Replace space with underscore.
	originalFilename = originalFilename.replace( new RegExp( " ", "g" ), "_" );

	// Fix camel-caseing.
	var newFilename = "" + originalFilename.charAt( 0 );
	for( var ci = 1; ci < originalFilename.length; ci++ ) {

		if( isUppercaseChar( originalFilename.charAt( ci ) ) && 
			isLowercaseChar( originalFilename.charAt( ci - 1 ) ) ) {
			newFilename += "_";	
		}
		newFilename += originalFilename.charAt( ci );
	}

	// Lowercase it.
	return originalFile.path + "/" + newFilename.toLowerCase();
};

// Uppercase check of char
function isUppercaseChar( ch ) {
	return (( ch.charCodeAt() >= 65 ) && ( ch.charCodeAt() <= 90 ));
};

// Lowercase check of char
function isLowercaseChar( ch ) {
	return (( ch.charCodeAt() >= 97 ) && ( ch.charCodeAt() <= 122 ));
};

/**
 * Executes the script (exporting).
 */
function execute() {

	var files = getIllustratorFilesInDir();
	if( files.length > 0 ) {
		if( !Window.confirm( 
				"Found " + files.length + " Illustrator file(s) for conversion, continue?", 
				false,
				"Run script..." ) ) {
			return;
		}
	}

	showDialogs( false );
	for( var cfi = 0; cfi < files.length; cfi++ ) {

		var svgFilename = exportFileAsSVG( files[ cfi ] );
		$.writeln( "Exported " + files[ cfi ].fullName + " -> " + svgFilename );		
	}

	showDialogs( true );
	if( files.length > 0 ) {
		alert( "File(s) exported to SVG successfully!" );	
	}
};

/**
 * Turns of the displaying of doalogs which causes the script to halt waiting fir user actions/inputs.
 * @param flag True/false flag for enabling/disabling the alerts.
 */
function showDialogs( flag ) {
	app.userInteractionLevel = flag ? UserInteractionLevel.DISPLAYALERTS : UserInteractionLevel.DONTDISPLAYALERTS;
};

// Run...
execute();
